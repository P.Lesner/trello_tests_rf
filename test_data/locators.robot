*** Variables ***
#endpoints
${home_page}  https://trello.com
${boards_page_endpoint}  /boards

#login page
${link_to_login_page}  Zaloguj
${user_field}  id=user
${password_field}  id=password
${login_button}  id=login
${email_field}  id=username
${submit_button}  id=login-submit

#main page
${main_page_title}  Tablice | Trello
${create_new_board_button}  class=mod-add
${trello_logo}  class=header-logo-default
${any_board}  xpath=//div[contains(@class,'content-all-boards')]/div[@class='boards-page-board-section mod-no-sidebar']/ul/li/a[contains(@class,'board-tile')]

#member menu
${member_menu}  css=button.js-open-header-member-menu
${logout_link}  xpath=//button[@data-test-id='header-member-menu-logout']

#logged out page
${logged_out_page_title}  Wylogowany z Trello
${text_on_logged_out_page}  Jesteś wylogowany. I co teraz?

#create new board page
${name_of_new_board_field}  class=subtle-input
${create_board_button}  class=primary
${set_visibility_button}  css=button.vis-chooser-trigger

#board menu
${show_more_option}  class=js-open-more
${close_board_option}  class=js-close-board
${confirm_close_board}  class=js-confirm
${delete_board_permanently_text}  link=Trwale usuń tablicę…
${open_board_menu_button}  class=mod-show-menu
${go_back_to_options_btn}  css=a.icon-back
${link_description}  css=div.u-gutter
${textrea_description}  css=div.description-edit.edit > textarea
${submit_description_btn}  css=input.primary.confirm.mod-submit-edit.js-save-edit
${description_content}  css=div.current.markeddown.hide-on-edit.js-desc.js-show-with-desc > p
${close_menu_btn}  css=a.board-menu-header-close-button

#privacy settings
${privacy_set_btn}  css=#permission-level > span.board-header-btn-text
${confirm_visibility_change_btn}  css=button.js-submit.wide.primary.full.make-public-confirmation-button

#add to favourites
${fav_star_on_board}  css=div.board-header.u-clearfix.js-board-header > a.board-header-btn.js-star-board
${fav_star_on_board_enabled}  .board-header-btn-enabled

#deleted board page
${board_not_found}  Tablicy nie znaleziono.

#error page
${error_trello}  Błąd | Trello

#cards
${name_of_new_card_textarea}  css=textarea.list-card-composer-textarea.js-card-title
${archive_card_btn}  css=a.quick-card-editor-buttons-item.js-archive
${card_title_textarea}  css=textarea.mod-card-back-title.js-card-detail-title-input
${close_card_options}  css=a.icon-md.icon-close.dialog-close-button.js-close-window
${base_list_selector1}  xpath=//h2[@class='list-header-name-assist js-list-name-assist' and contains(text(),'
${base_list_selector2}  ')]/parent::div/parent::div//
${move_card_option}  css=a.quick-card-editor-buttons-item.js-move-card
${copy_card_option}  css=a.quick-card-editor-buttons-item.js-copy-card
${select_board}  css=select.js-select-board
${select_list}  css=select.js-select-list
${select_position}  css=select.js-select-position
${confirm_action_card_btn}  css=input.primary.wide.js-submit
${cancel_add_card_btn}  css=a.icon-lg.icon-close.dark-hover.js-cancel

#lists
${add_new_list_btn}  css=div.js-add-list.list-wrapper.mod-add.is-idle > form > a.open-add-list.js-open-add-list
${new_list_name_input}  css=input.list-name-input
${save_new_list_btn}  css=input.primary.mod-list-add-button.js-save-edit
${list_menu_btn}  a[contains(@class, 'list-header-extras-menu')]
${close_list_menu_btn}  css=a.pop-over-header-close-btn
${archive_list_opt}  css=a.js-close-list