*** Settings ***
Library          RequestsLibrary
Library          Collections
Resource         fe_variables.robot

*** Keywords ***
Create board
    [Arguments]  ${board_name}
    create session  trello   https://api.trello.com/1/boards/
    ${create_board}=  post request  trello  ?name=${board_name}${authentication_token_key}
    log to console  ${create_board.json()}
    ${boards_id}=  get from dictionary  ${create_board.json()}  id
    log to console  ${boards_id}
    set test variable  ${boards_id}

Delete board
    [Arguments]  ${boards_id}
    ${delete_board}=  delete request  trello  ${boards_id}${authentication_key}
    log to console  ${delete_board.json()}

