*** Settings ***
Library          SeleniumLibrary
Library          RequestsLibrary
Library          Collections
Resource         ../test_data/fe_keywords.robot

Test Setup       Run keywords    Open browser and login
...                       AND    Generate name of  board
...                       AND    Create new board  ${board_name}  private
...                       AND    Reload page
...                       AND    Generate name of  list
...                       AND    Create list on board  ${list_name}

Test Teardown    Run keywords    Reload page
...                       AND    Delete current board
...                       AND    Logoout and close browser

*** Keywords ***
*** Variables ***
*** Test Cases ***

CT001. Add card to the list
    [Tags]  Basic  Cards  FE
    Add new card on the list  ${list_name}  default

CT002. Archive card
    [Tags]  Cards  FE
    Add new card on the list  ${list_name}  test002
    Archive card from the list  ${list_name}  ${card_name}

CT003. Rename card
    [Tags]  Cards  FE
    Add new card on the list  ${list_name}  test003
    Rename card from the list  ${list_name}  ${card_name}  ${card_name}b

CT004. Move card to another list
    [Tags]  Cards  FE
    Add new card on the list  ${list_name}  ca004
    Create list on board  list2
    Add new card on the list  list2  name7
    Expand card options  ${list_name}  ca004
    wait until element is visible  ${move_card_option}
    click element  ${move_card_option}
    Choose board  ${board_name} (aktualny)
    Choose list  list2
    Choose position  1
    click element  ${confirm_action_card_btn}
    Reload page
    Check if card is on  list2  ca004  1

CT005. Cancel addition of new card
    [Tags]  Cards  FE
    Generate name of  card
    ${add_card_btn} =  Generate link to card  ${list_name}  ${card_name}  add_card_btn
    click element  ${add_card_btn}
    wait until element is visible  ${name_of_new_card_textarea}
    input text  ${name_of_new_card_textarea}  ${card_name}
    click element  ${cancel_add_card_btn}
    ${canceled_card} =  Generate link to card  ${list_name}  ${card_name}  card_link
    element should not be visible  ${canceled_card}

CT006. Copy card to another list
    [Tags]  Cards  FE
    Add new card on the list  ${list_name}  ca004
    Create list on board  list2
    Add new card on the list  list2  name7
    Expand card options  ${list_name}  ca004
    wait until element is visible  ${copy_card_option}
    click element  ${copy_card_option}
    Choose board  ${board_name} (aktualny)
    Choose list  list2
    Choose position  1
    click element  ${confirm_action_card_btn}
    Reload page
    Check if card is on  list2  ca004  1
    Check if card is on  ${list_name}  ca004  1