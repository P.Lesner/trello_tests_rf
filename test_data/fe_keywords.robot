*** Settings ***
Library          robot.libraries.DateTime
Library          Collections
Resource         locators.robot
Resource         fe_variables.robot
Resource         credentials.robot

*** Keywords ***
Click login button
    Click link  ${link_to_login_page}
    wait until element is visible  ${user_field}
    Page should contain element  ${user_field}
    Page should contain element  ${password_field}
    Page should contain element  ${login_button}
    Page should contain button  ${login_button}

Login user
    [Arguments]  ${email_login}  ${password}
    Input text  ${user_field}  ${email_login}
    wait until element is not visible  ${password_field}
    Click button  ${login_button}
    wait until element is visible  ${email_field}
    Input text  ${email_field}  ${email_login}
    click button  ${submit_button}
    wait until element is visible  ${password_field}
    Input text  ${password_field}  ${password}
    click button  ${submit_button}

Check if user is logged in
    wait until keyword succeeds  5x  2s  title should be  ${main_page_title}

Logout user
    click button  ${member_menu}
    Click button  ${logout_link}

Check if user is logged out
    Wait until keyword succeeds  5x  2s  title should be  ${logged_out_page_title}
    Page should contain  ${text_on_logged_out_page}

Check if URL is equal to
    [Arguments]  ${URL}
    Location should be  ${URL}

Create new board
    [Arguments]  ${name_of_board}  ${visibility}=default
    set test variable  ${name_of_board}  ${name_of_board}
    Click element  ${create_new_board_button}
    Check if URL is equal to  ${home_page}/${login}${boards_page_endpoint}
    Element should be disabled  ${create_board_button}
    Input text  ${name_of_new_board_field}  ${name_of_board}
    run keyword if  '${visibility}'!='default'  Set visiblity of board  ${visibility}
    Element should be enabled  ${create_board_button}
    Click element  ${create_board_button}
    Check if page title is equal to  ${name_of_board} | Trello

Set visiblity of board
    [Arguments]  ${status}
    click button  ${set_visibility_button}
    run keyword if  '${status}'=='private'  click element  xpath=//*[@id='chrome-container']/div[4]/div/div[2]/div/ul/li[1]/a
    run keyword if  '${status}'=='public'  click element  xpath=//*[@id='chrome-container']/div[4]/div/div[2]/div/ul/li[2]/a
    run keyword if  '${status}'=='public'  click element  xpath=//input[@class='js-confirm full primary']

Check if page title is equal to
    [Arguments]  ${title}
    Wait until keyword succeeds  5x  2s  title should be  ${title}

Delete current board
    Open menu
    Show more option
    Click delete board option
    Confirm deletion of board
    Confirm definitely deletion of board
    Confirm deletion of board
    Check if user is on error page

Open menu
    run keyword and ignore error  wait until element is visible  ${open_board_menu_button}
    run keyword and ignore error  Click element  ${open_board_menu_button}

Show more option
    Return to options
    Click element  ${show_more_option}

Return to options
    sleep  2s
    ${is_return_to_opt_btn_visible}=  Run Keyword And Return Status  click element  ${go_back_to_options_btn}

Click delete board option
    Wait until keyword succeeds  5x  2s  click element  ${close_board_option}

Confirm deletion of board
    Wait until keyword succeeds  5x  2s  click element  ${confirm_close_board}

Confirm definitely deletion of board
    wait until element is visible  ${delete_board_permanently_text}
    Click link  ${delete_board_permanently_text}

Check if user is on error page
    Wait until keyword succeeds  5x  2s  page should contain  ${board_not_found}
    Title should be  ${error_trello}

Add description to the board
    [Arguments]  ${description}
    wait until element is visible  ${link_description}
    Click element  ${link_description}
    Input text  ${textrea_description}  ${description}
    Click element  ${submit_description_btn}
    set test variable  ${set_description}  ${description}

Check if the board has description
    ${description_on_page} =  get text  ${description_content}
    should be equal  ${set_description}  ${description_on_page}

Open privacy settings
    wait until element is visible  ${privacy_set_btn}
    click element  ${privacy_set_btn}

Change visibility to
    [Arguments]  ${visibility}
    set test variable  ${visibility}  ${visibility}
    privacy setting option  ${visibility}
    wait until element is visible  ${privacy_setting_option}
    click element  ${privacy_setting_option}

Check if visibility is set to
    [Arguments]  ${visibility}
    set test variable  ${visibility}  ${visibility}
    privacy setting marker  ${visibility}
    element should be visible  ${privacy_setting_marker}

Confirm change of visibility
    wait until element is visible  ${confirm_visibility_change_btn}
    click element  ${confirm_visibility_change_btn}

privacy setting marker
    [Arguments]  ${visibility}
    ${privacy_setting_marker} =  set variable  xpath=//a[@class='js-select' and @name='${visibility}']/span[@class='icon-sm icon-check']
    set test variable  ${privacy_setting_marker}  ${privacy_setting_marker}

privacy setting option
    [Arguments]  ${visibility}
    ${privacy_setting_option} =  set variable  css=a[name=${visibility}]
    set test variable  ${privacy_setting_option}  ${privacy_setting_option}

Add board to favourites
    wait until element is visible  ${fav_star_on_board}
    wait until page contains element  ${fav_star_on_board}
    click element  ${fav_star_on_board}
    wait until element is visible  ${fav_star_on_board}${fav_star_on_board_enabled}

Add board to favourites on main page
    generate link to board from main page
    wait until page contains element  ${link_to_board}
    mouse over  ${link_to_board}
    generate link to favicon from main page
    wait until element is visible  ${link_to_favicon}
    click element  ${link_to_favicon}

Remove board from favourites on board page
    wait until element is visible  ${fav_star_on_board}${fav_star_on_board_enabled}
    click element  ${fav_star_on_board}${fav_star_on_board_enabled}

Remove board from favourites on main page
    generate link to favicon from favs
    wait until element is visible  ${link_to_favicon}
    click element  ${link_to_favicon}


Check on main page if board is in favourites
    Go to subpage  boards
    generate link to board from favourites section
    wait until element is visible  ${link_to_board}

Check on main page if board is not in favourites
    Go to subpage  boards
    generate link to board from favourites section
    element should not be visible  ${link_to_board}

Go to subpage
    [Arguments]  ${uri}
    go to  ${home_page}/${login}/${uri}

Generate name of
    [Arguments]  ${element}
    ${date_and_time} =  Get current date  result_format=datetime
    ${date_conv} =  set variable  ${date_and_time.year}${date_and_time.month}${date_and_time.day}
    ${time_conv} =  set variable  ${date_and_time.hour}${date_and_time.minute}${date_and_time.second}
    run keyword if  '${element}'=='board'  set test variable  ${board_name}  testname${date_conv}${time_conv}
    run keyword if  '${element}'=='list'  set test variable  ${list_name}  li${time_conv}
    run keyword if  '${element}'=='card'  set test variable  ${card_name}  ca${time_conv}

generate link to board from main page
    ${link_to_board} =  set variable  xpath=//li[@class='boards-page-board-section-list-item']/a[contains(@href,'${board_name}') and @class='board-tile mod-light-background']
    set test variable  ${link_to_board}  ${link_to_board}

generate link to board from favourites section
    ${link_to_board} =  set variable  xpath=//li[@class='boards-page-board-section-list-item js-draggable']/a[contains(@href,'${board_name}') and @class='board-tile mod-light-background']
    set test variable  ${link_to_board}  ${link_to_board}

generate link to favicon from main page
    ${link_to_favicon} =  set variable  xpath=//a[contains(@href,'${board_name}')]/div/div[@class='board-tile-details-sub-container']/span/span
    set test variable  ${link_to_favicon}  ${link_to_favicon}

generate link to favicon from favs
    ${link_to_favicon} =  set variable  xpath=//a[contains(@href,'${board_name}')]/div/div[2]/span/span
    set test variable  ${link_to_favicon}  ${link_to_favicon}

Go to board page
    [Arguments]  ${board_name}  ${from_where}
    run keyword if  '${from_where}'=='from all'  generate link to board from main page
    run keyword if  '${from_where}'=='from favourites'  generate link to board from favourites section
    wait until element is visible  ${link_to_board}
    ${board_url} =  get element attribute  ${link_to_board}  href
    click element  ${link_to_board}
    check if url is equal to  ${board_url}
    Check if page title is equal to  ${board_name} | Trello

Delete all boards
    :FOR    ${i}    IN RANGE    999999
    \    Go to subpage  boards
    \    run keyword and return status  wait until element is visible  ${any_board}
    \    ${is_board_visible} =  Run Keyword And Return Status  element should not be visible  ${any_board}
    \    Exit For Loop If  ${is_board_visible}
    \    click element  ${any_board}
    \    wait until element is not visible  ${any_board}
    \    Delete current board
    Log    Exited

Count lists on board
    ${lists_counter} =  get element count  ${add_new_card_btn}
    set test variable  ${lists_counter}  ${lists_counter}
    log  ${lists_counter}

Create list if there is none
    run keyword if  ${lists_counter}==0  Generate name of  list
    run keyword if  ${lists_counter}==0  Create list  ${list_name}

Create list on board
    [Arguments]  ${list_name}
    ${status} =  run keyword and return status  wait until element is visible  ${add_new_list_btn}
    run keyword if  '${status}'=='True'  Create list when btn is visible  ${list_name}
    run keyword if  '${status}'=='False'  Create list when btn is already clicked  ${list_name}

Create list when btn is visible
    [Arguments]  ${list_name}
    click link  ${add_new_list_btn}
    input text  ${new_list_name_input}  ${list_name}
    click element  ${save_new_list_btn}
    wait until page contains  ${list_name}

Create list when btn is already clicked
    [Arguments]  ${list_name}
    input text  ${new_list_name_input}  ${list_name}
    click element  ${save_new_list_btn}
    wait until page contains  ${list_name}

Add new card on the list
    [Arguments]  ${name_of_the_list}  ${card_name}
    run keyword if  '${card_name}'=='default'  Generate name of  card
    run keyword if  '${card_name}'!='default'  set test variable  ${card_name}  ${card_name}
    ${add_card_btn} =  Generate link to card  ${name_of_the_list}  ${card_name}  add_card_btn
    click element  ${add_card_btn}
    wait until element is visible  ${name_of_new_card_textarea}
    input text  ${name_of_new_card_textarea}  ${card_name}
    ${confirm_card_btn} =  Generate link to card  ${name_of_the_list}  ${card_name}  confirm_card_btn
    click element  ${confirm_card_btn}
    ${added_card} =  Generate link to card  ${name_of_the_list}  ${card_name}  card_link
    wait until element is visible  ${added_card}

Archive card from the list
    [Arguments]  ${name_of_the_list}  ${card_name}
    Expand card options  ${name_of_the_list}  ${card_name}
    click element  ${archive_card_btn}
    ${archived_card} =  Generate link to card  ${name_of_the_list}  ${card_name}  card_link
    wait until element is not visible  ${archived_card}

Expand card options
    [Arguments]  ${name_of_the_list}  ${card_name}
    ${expand_options} =  Generate link to card  ${name_of_the_list}  ${card_name}  expand_card_options
    ${card_link} =  Generate link to card  ${name_of_the_list}  ${card_name}  card_link
    mouse over  ${card_link}
    mouse over  ${expand_card_options}
    click element  ${expand_card_options}

Rename card from the list
    [Arguments]  ${name_of_the_list}  ${card_name}  ${new_card_name}
    Generate link to card  ${name_of_the_list}  ${card_name}  card_link
    wait until element is visible  ${card_link}
    click element  ${card_link}
    wait until element is visible  ${card_title_textarea}
    click element  ${card_title_textarea}
    input text  ${card_title_textarea}  ${new_card_name}
    wait until element is visible  ${close_card_options}
    click element  ${close_card_options}
    ${new_card_link} =  Generate link to card  ${name_of_the_list}  ${card_name}  card_link
    wait until element is visible  ${new_card_link}

Generate link to card
    [Arguments]  ${name_of_the_list}  ${card_name}  ${option}
    ${base_list_selector} =  set variable  ${base_list_selector1}${name_of_the_list}${base_list_selector2}
    ${add_card_btn} =  set variable  ${base_list_selector}a[contains(@class,'open-card-composer')]
    ${card_link} =  set variable  ${base_list_selector}span[contains(text(),'${card_name}') and contains(@class,'list-card-title js-card-name')]
    ${expand_card_options} =  set variable  ${card_link}/parent::div/parent::a/span
    ${confirm_card_btn} =  set variable  ${base_list_selector}input[@class='primary confirm mod-compact js-add-card']
    run keyword if  '${option}'=='add_card_btn'  set test variable  ${add_card_btn}  ${add_card_btn}
    return from keyword if  '${option}'=='add_card_btn'  ${add_card_btn}
    run keyword if  '${option}'=='card_link'  set test variable  ${card_link}  ${card_link}
    return from keyword if  '${option}'=='card_link'  ${card_link}
    run keyword if  '${option}'=='expand_card_options'  set test variable  ${expand_card_options}  ${expand_card_options}
    return from keyword if  '${option}'=='expand_card_options'  ${expand_card_options}
    run keyword if  '${option}'=='confirm_card_btn'  set test variable  ${confirm_card_btn}  ${confirm_card_btn}
    return from keyword if  '${option}'=='confirm_card_btn'  ${confirm_card_btn}

Choose board
    [Arguments]  ${board_name}
    Sleep  1s
    click element  ${select_board}
    ${chosen_option} =  set variable  xpath=//option[contains(text(),'${board_name}')]
    wait until element is visible  ${chosen_option}
    click element  ${chosen_option}

Choose list
    [Arguments]  ${name_of_the_list}
    click element  ${select_list}
    ${chosen_option} =  set variable  xpath=//option[contains(text(),'${name_of_the_list}')]
    wait until element is visible  ${chosen_option}
    click element  ${chosen_option}

Choose position
    [Arguments]  ${position}
    click element  ${select_position}
    ${chosen_option} =  set variable  xpath=//select[@class='js-select-position']/option[${position}]
    wait until element is visible  ${chosen_option}
    click element  ${chosen_option}

Check if card is on
    [Arguments]  ${name_of_the_list}  ${card_name}  ${position}
    wait until element is not visible  ${confirm_action_card_btn}
    ${card_link} =  set variable  xpath=//textarea[contains(text(),'${name_of_the_list}')]/parent::div/following-sibling::div/a[${position}]//span[@class='list-card-title js-card-name']
    ${card_on_list_txt} =  get text  ${card_link}
    should contain  ${card_on_list_txt}  ${card_name}

Open browser and login
    Open Browser  ${home_page}  ${BROWSER}
    Maximize browser window
    Click login button
    Login user  ${email}  ${password}
    Check if user is logged in

Logoout and close browser
    Logout user
    Check if user is logged out
    Close Browser

Delete list
    [Arguments]  ${list_name}
    Open list menu  ${list_name}
    Choose list option  archive

Check if list is no longer visible on board
    [Arguments]  ${list_name}
    ${list_is_visible} =  run keyword and return status  Open list menu  ${list_name}
    should be equal  ${list_is_visible}  ${False}

Choose list option
    [Arguments]  ${option}
    run keyword if  '${option}'=='archive'  set test variable  ${opt}  ${archive_list_opt}
    wait until element is visible  ${opt}
    wait until keyword succeeds  60s  1s  click element  ${opt}

Open list menu
    [Arguments]  ${name_of_the_list}
    ${list_menu} =  set variable  ${base_list_selector1}${name_of_the_list}${base_list_selector2}${list_menu_btn}
    wait until element is visible  ${list_menu}
    click element  ${list_menu}
    wait until element is visible  ${close_list_menu_btn}

Drag list and put on new place
    [Arguments]  ${list_name_1}  ${list_name_2}
    ${list_menu_1} =  set variable  ${base_list_selector1}${list_name_1}${base_list_selector2}${list_menu_btn}
    ${list_menu_2} =  set variable  ${base_list_selector1}${list_name_2}${base_list_selector2}${list_menu_btn}
    drag and drop  ${list_menu_1}  ${list_menu_2}

List position should be
    [Arguments]  ${list_number}  ${list_name}
    ${list_name_element} =  set variable  xpath=//div[${list_number}][@class='js-list list-wrapper']//textarea[contains(@class,'list-header-name') and contains(text(),'${list_name}')]
    element should be visible  ${list_name_element}