*** Settings ***
Library          SeleniumLibrary
Library          RequestsLibrary
Library          Collections
Resource         ../test_data/fe_variables.robot
Resource         ../test_data/credentials.robot
Resource         ../test_data/fe_keywords.robot
Resource         ../test_data/api_keywords.robot

Test Setup       Run keywords    Open browser and login
Test Teardown    Run keywords    Logoout and close browser

*** Keywords ***
*** Variables ***
*** Test Cases ***

BT001. Login user using email and logout user.
    [Tags]  Basic  Login  FE
    [Setup]  Open Browser    ${home_page}    ${BROWSER}
    Maximize browser window
    Click login button
    Login user  ${email}  ${password}
    Check if user is logged in

#BT002. Login user using login and logout user.
#    [Tags]  Basic  Login  FE
#    [Setup]  Open Browser    ${home_page}    ${BROWSER}
#    Maximize browser window
#    Click login button
#    Login user  ${login}  ${password}
#    Check if user is logged in

BT003. Create and delete board.
    [Tags]  Basic  Board  FE
    Create new board  klops1234
    Delete current board

BT004. Create and delete board using API.
    [Tags]  Basic  Board  API
    [Setup]
    [Teardown]
    Create board  board_cr_api_1
    Delete board  ${boards_id}