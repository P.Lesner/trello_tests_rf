*** Settings ***
Library          SeleniumLibrary
Library          RequestsLibrary
Library          Collections
Library          robot.libraries.DateTime
Resource         ../test_data/fe_keywords.robot

Test Setup       Run keywords    Open browser and login
Test Teardown    Run keywords    Logoout and close browser
Suite Teardown    Run keywords    Close all browsers

*** Keywords ***
*** Variables ***
*** Test Cases ***
BO001. Create private board
    [Tags]  Basic  Board  FE
    Create new board  name1234  private
    Delete current board

BO002. Create public board
    [Tags]  Board  FE
    Create new board  name1234  public
    Delete current board

BO003. Add description to the board
    [Tags]  Board  FE
    Create new board  name1234  public
    Open menu
    Add description to the board  description 12345 ?@#$%^&*
    Check if the board has description
    Delete current board

BO004. Change visibility of the board
    [Tags]  Board  FE
    Create new board  name1234  public
    Open privacy settings
    Change visibility to  private
    Open privacy settings
    Check if visibility is set to  private
    Change visibility to  public
    Confirm change of visibility
    Open privacy settings
    Check if visibility is set to  public
    Delete current board

BO005. Add board to favourites from board page
    [Tags]  Board  FE
    Generate name of  board
    Create new board  ${board_name}  private
    Add board to favourites
    Check on main page if board is in favourites
    Go to board page  ${board_name}  from favourites
    Remove board from favourites on board page
    Check on main page if board is not in favourites
    Go to board page  ${board_name}  from all
    Delete current board

BO006. Add board to favourites from main page
    [Tags]  Board  FE
    Generate name of  board
    Create new board  ${board_name}  private
    Go to subpage  boards
    Add board to favourites on main page
    reload page
    Check on main page if board is in favourites
    Remove board from favourites on main page
    Check on main page if board is not in favourites
    Go to board page  ${board_name}  from all
    Delete current board

BO007. Delete all boards
    [Tags]  Board  FE
    Delete all boards