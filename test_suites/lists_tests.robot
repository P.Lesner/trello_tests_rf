*** Settings ***
Library          SeleniumLibrary
Library          RequestsLibrary
Library          Collections
Library          robot.libraries.DateTime
Resource         ../test_data/fe_keywords.robot

Test Setup       Run keywords    Open browser and login
...                       AND    Create new board  liststest  private

Test Teardown    Run keywords    Delete current board
...                       AND    Logoout and close browser

Suite Teardown    Run keywords    Close all browsers

*** Keywords ***
*** Variables ***
*** Test Cases ***
LI001. Create list on board
    [Tags]  Basic  List  FE
    Generate name of  list
    Reload page
    Create list on board  ${list_name}

LI002. Delete list from board
    [Tags]  Basic  List  FE
    Generate name of  list
    Reload page
    Create list on board  ${list_name}
    Delete list  ${list_name}
    Check if list is no longer visible on board  ${list_name}

LI003. Change order of lists
    [Tags]  Basic  List  FE
    Generate name of  list
    Reload page
    Create list on board  ${list_name}1
    Create list on board  ${list_name}2
    Create list on board  ${list_name}3
    Reload page
    Drag list and put on new place  ${list_name}1  ${list_name}2
    List position should be  1  ${list_name}2
    List position should be  2  ${list_name}1
    List position should be  3  ${list_name}3
    Drag list and put on new place  ${list_name}3  ${list_name}1
    List position should be  1  ${list_name}2
    List position should be  2  ${list_name}3
    List position should be  3  ${list_name}1
    Drag list and put on new place  ${list_name}2  ${list_name}3
    List position should be  1  ${list_name}3
    List position should be  2  ${list_name}2
    List position should be  3  ${list_name}1