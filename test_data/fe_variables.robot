*** Settings ***
Resource         credentials.robot

*** Variables ***
${BROWSER}  Chrome
#${BROWSER}  headlesschrome

${authentication_token_key}  &defaultLabels=true&defaultLists=true&keepFromSource=none&prefs_permissionLevel=private&prefs_voting=disabled&prefs_comments=members&prefs_invitations=members&prefs_selfJoin=true&prefs_cardCovers=true&prefs_background=blue&prefs_cardAging=regular&key=${trello_key}&token=${trello_token}
${authentication_key}  ?key=${trello_key}&token=${trello_token}